package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID,using="createLeadForm_companyName")
	WebElement eleEnterCompanyName;
	@FindBy(how=How.ID,using="createLeadForm_firstName")
	WebElement eleEnterFirstName;
	@FindBy(how=How.ID,using="createLeadForm_lastName")
	WebElement eleEnterLastName;
	@FindBy(how=How.NAME,using="submitButton")
	WebElement eleClickCreateLeadBtn;
	
	
	
	public CreateLeadPage enterCompanyName(String data) {
		clearAndType(eleEnterCompanyName, data);
		return this;
	}
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleEnterFirstName, data);
		return this;
	}
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleEnterLastName, data);
		return this;
	}
	public ViewLeadsPage eleClickCreateLeadButton() {
		clickWithNoSnap(eleClickCreateLeadBtn);
		return new ViewLeadsPage();
	}
	
	
}
