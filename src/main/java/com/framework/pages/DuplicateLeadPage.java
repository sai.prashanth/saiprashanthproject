package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods {

	public DuplicateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH,using="//input[@class='smallSubmit']")
	WebElement eleClickCreateLeadBtnInDupLead;
	
	public DuplicateLeadPage verifyTitleAsDuplicateLead(String data) {
		verifyTitle(data);
		return this;
	}
	public ViewLeadsPage clickCreateLeadInDupLead() {
		clickWithNoSnap(eleClickCreateLeadBtnInDupLead);
		return new ViewLeadsPage();
	}
}
