package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadsPage extends ProjectMethods {

	public ViewLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID,using="viewLead_firstName_sp")
	WebElement eleVerifyFirstName;
	
	@FindBy(how=How.XPATH,using="//a[@class='subMenuButton']")
	WebElement eleDuplicateLeadBtn;
	
	@FindBy(how=How.LINK_TEXT,using="Edit")
	WebElement eleEditButton;
	
	@FindBy(how=How.ID,using="viewLead_companyName_sp")
	WebElement eleVerifyCompanyName;
		
	public ViewLeadsPage verifyFirstName(String data) {
		verifyExactText(eleVerifyFirstName, data);
		return this;
	}
	public DuplicateLeadPage clickDuplicateLead() {
		clickWithNoSnap(eleDuplicateLeadBtn);
		return new DuplicateLeadPage();
	}
	
	public EditLeadPage clickEditButton() {
		clickWithNoSnap(eleEditButton);
		return new EditLeadPage();
	}
	
	public ViewLeadsPage verifyTitleAfterFindLead(String data) {
		verifyTitle(data);
		return this;
	}
	public ViewLeadsPage verifyUpdatedCompanyName(String data) {
		verifyPartialText(eleVerifyCompanyName, data);
		return this;
	}

}
